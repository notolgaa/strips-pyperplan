
"""
Implements the STRIPS search algorithm.
"""

import logging

from search import searchspace
from pddl import parser
from task import Task
from collections import deque

def strips(planning_task):

    plan = {}
    iteration = 0
    current_state = planning_task.initial_state
    action = []
    goal = []
    action = parse_action_stmt()
    goal = planning_task.goal_state


    while iteration <= len(goal):
        
        logging.debug("strips_search: Iteration %d" % (iteration+1))
        g = goal[iteration]
        j = 0
        for j in len(action):
            if g == current_state.append(action[j].precond):
                break
            else:
                j += 1
        current_state = current_state.append(action[j].effect)
        plan = plan.append(action[j].name)
        iteration += 1

    if planning_task.goal_reached(current_state):
        return plan
    else:
        logging.info("No operators left. Task unsolvable.")
  
    return None
